
This repository contains notebooks and links for the Teaching Interest Group
meeting I'm leading at Queen Mary University of London on July 5th 2020.

It is best experienced as a binder, click here: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Ffsmeraldi%2Ftigrnotebooks%2Fsrc%2Fmaster/master)

**Some useful links:**

* https://jupyter.org/ home of Jupyter notebooks

* https://www.anaconda.com/ a Python distribution including notebooks and lots of scientific libraries, a great starting kit for Windows users

* https://nbviewer.jupyter.org/ the simplest way to get a notebook online (non-interactive), see this [example](https://nbviewer.jupyter.org/url/www.eecs.qmul.ac.uk/~fabri/nonZim/iPython/Regular_expressions.ipynb)

* https://mybinder.org/ virtual machines made trivial. A truly amazing way of sharing your notebooks in the cloud

* https://colab.research.google.com/ Google-doc style cooperation for notebooks. Fully interactive, access to TensorFlow and GPUs, but it can be painfully slow

* Finally, for those of you who'd rather do another language, Jupyter notebooks offer a variety of [kernels](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels). Here is a sample binder I put together from various online sources using the iJava kernel: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Ffsmeraldi%2Fbrynmawrjava%2Fsrc%2Fmaster/master) (the git repo is [here](https://bitbucket.org/fsmeraldi/brynmawrjava/src/master/))

(C) Fabrizio Smeraldi, June 2020 - All rights reserved.

