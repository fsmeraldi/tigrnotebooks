{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the few things computers are really good at is repeating the same actions over and over again (possibly on different data). This is achieved with iterative constructs (**loops**). This notebooks introduces loops; combining these instructions with conditional statements and the advanced data structures we covered in the first few lessons, you will be able to write  useful code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# For loop"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a control structure that allows you to repeat a block, while an index variable takes on a predefined set of values. Formally we write this as:\n",
    "```\n",
    "for VAR in ITERABLE:\n",
    "    BLOCK\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An ITERABLE is essentially an ordered collection of items (think a list, a tuple or a file). Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "codons=['ACT','ACG','ACA','ACC']\n",
    "for c in codons:\n",
    "    print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, note the indentation of the block which is mandatory.\n",
    "\n",
    "In other languages *for* loops use an index variable that iterates over a range of values. This can be done in Python by iterating over a list of numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a=[0,1,2,3,4]\n",
    "for i in a:\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the convenient function ``range``: this is an immutable sequence of numbers that's computed lazily as needed. This means that elements of the sequence are computed one at a time as they are used; the advantage is that all ranges take up the same space in memory regardless of their length - only the starting point, end point and optional step are stored (see the [documentation](https://docs.python.org/3/library/stdtypes.html#range) for more info). We won't worry about the details here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(range(0,5)) # told you, this guy is lazy\n",
    "print(list(range(0,5))) # this forces computation of all elements right away\n",
    "print(list(range(5))) # we can be lazy too, and leave the 0 out!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a=range(0,5) # laziness is good...\n",
    "for i in a:  # ... each element computed as needed\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or even more simply:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(0,5): \n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we have seen, we can iterate over a list. However, sometimes we may want to iterate over a list explicitly using an index, maybe because we have more than one list at the same time. This is a very typical use of the index variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "codons=['ACT','CCT','CTT','TAT']\n",
    "amino=['Thr', 'Pro', 'Leu', 'Tyr']\n",
    "for i in range(0,4):\n",
    "    print(codons[i] + \" encodes for \"+ amino[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, Python also gives us an option to create a \"composite\" list and iterate over that one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "codons=['ACT','CCT','CTT','TAT']\n",
    "amino=['Thr', 'Pro', 'Leu', 'Tyr']\n",
    "for (c,a) in zip(codons, amino):\n",
    "    print(c + \" encodes for \"+ a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In another example, you can step through a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peptide=\"ATPSF\"\n",
    "names={'A': 'Ala', 'T': 'Thr', 'P': 'Pro', 'S': 'Ser', 'F': 'Phe'}\n",
    "for p in peptide:\n",
    "    # 'end' parameter replaces final newline with space\n",
    "    print (names[p], end=\" \") \n",
    "print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another classic example is the multiplication table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "num_str=input(\"Give me a number: \")\n",
    "num=int (num_str)\n",
    "for i in range(0,11):\n",
    "        print (num, \"*\", i, \"=\", num*i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Nested loops"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that loops can be **nested** by placing one loop inside the other. Then the inner loop runs its entire course over and over again as specified by the outer loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(1,11):\n",
    "        for j in range (1,11):\n",
    "            # print(i*j, end=\" \") # spacing is a bit wonky\n",
    "            # Fancy \"{:3d} \".format() syntax to get the right spacing\n",
    "            print(\"{:3d} \".format(i*j), end= \" \")\n",
    "        print() # only runs once for each value of i"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# While loop"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This other type of loop does not iterate over an object such as a list, but keeps looping until a certain condition becomes false. The syntax is:\n",
    "```\n",
    "while EXPRESSION:\n",
    "    BLOCK\n",
    "```\n",
    "where *EXPRESSION* is a Boolean expression of the type we saw previously in conjunction with *if/else*.\n",
    "\n",
    "Since there is no predefined object to iterate on, this type of loop comes in handy when we do not know in advance how many objects we need to process - say when reading a file, or asking the user for input.\n",
    "\n",
    "Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Enter numbers or 'sum' to display the total\")\n",
    "total=0.0 # this is called an \"accumulator\"\n",
    "userin=input(\">> \")\n",
    "while userin!=\"sum\":\n",
    "    num=float(userin)\n",
    "    total+=num\n",
    "    userin=input(\">> \")\n",
    "print(\"The total is: \", total)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In other situations, we might genuinely not know where we should stop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "power=1\n",
    "while power<=1000:\n",
    "    power*=2\n",
    "print (\"The lowest power of 2 greater than 1000 is\", power)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, closer to home:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stop=['TAG', 'TAA', 'TGA']\n",
    "seq= \"GGACTGGCTTGGGTATAATGG\"\n",
    "plen=0\n",
    "pos=0\n",
    "while seq[pos:pos+3] not in stop:\n",
    "    plen+=1\n",
    "    pos+=3\n",
    "print(\"Protein length:\", plen)\n",
    "print(\"Open reading frame:\", seq[0:pos])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"AAC\" in \"BAACTG\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The \"break\" statement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, it is convenient to break out of a for loop when a condition is met. For example, during a search, we may be happy with the first match and economise on computer resources by not looking any further. The **break** statement allows us to exit a loop there and then (and as such, is normally found inside an *if* clause):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peptides=[\"HQNKDE\",\"FYMILT\", \"RCHWSP\", \"QHYMILM\"]\n",
    "motif=input(\"Enter motif: \")\n",
    "hit=\"\"\n",
    "# perform a \"linear search\"\n",
    "for p in peptides:\n",
    "    if motif in p: # this check is time consuming\n",
    "        hit=p\n",
    "        break # no need to look any further\n",
    "        \n",
    "# notice that unless we have found a match, hit is untouched\n",
    "if hit!=\"\":\n",
    "    print(\"Found in: \", hit)\n",
    "else:\n",
    "    print(\"Not found\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may also want to *break* out of a while loop, which sometimes leads to elegant solutions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Enter numbers or 'sum' to display the total\")\n",
    "total=0.0 # this is called an \"accumulator\"\n",
    "while True: # forever!\n",
    "    userin=input(\">> \")\n",
    "    if userin==\"sum\":\n",
    "        break\n",
    "    num=float(userin)\n",
    "    total+=num\n",
    "print(\"The total is: \", total)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(C) 2014,2020 Fabrizio Smeraldi** ([f.smeraldi@qmul.ac.uk](mailto:f.smeraldi@qmul.ac.uk) - [web](http://www.eecs.qmul.ac.uk/~fabri/)), all rights reserved. In: \"Coding for Scientists\", School of Biological and Chemical Sciences, Queen Mary University of London."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
